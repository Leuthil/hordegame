﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class NetworkEntity : MonoBehaviour
{
    public ushort networkId;
    public ushort type;
    public ushort health;
    public ushort shield;
    public ushort targetId;
    public byte statuses;
    public ushort x;
    public ushort z;

    // this is probably not really relevant anymore
    //public ushort x
    //{
    //    get
    //    {
    //        return QuantizerHelper.PositionXFloatToUShort(transform.position.x);
    //    }
    //    set
    //    {
    //        transform.position = new Vector3(QuantizerHelper.PositionXUShortToFloat(value), transform.position.y, transform.position.z);
    //    }
    //}

    //public ushort z
    //{
    //    get
    //    {
    //        return QuantizerHelper.PositionXFloatToUShort(transform.position.z);
    //    }
    //    set
    //    {
    //        transform.position = new Vector3(transform.position.x, transform.position.y, QuantizerHelper.PositionXUShortToFloat(value));
    //    }
    //}
}