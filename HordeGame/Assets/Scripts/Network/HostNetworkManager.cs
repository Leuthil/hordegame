﻿using UnityEngine;
using System.Collections;
using Facepunch.Steamworks;
using System;
using System.Collections.Generic;

public class HostNetworkManager : COTANetworkManager
{
    public static HostNetworkManager Instance;

    protected List<HostNetworkEntity> spawnedEntities = new List<HostNetworkEntity>();
    protected List<ushort> destroyedNetworkIds = new List<ushort>();
    protected List<HostNetworkEntity> replicatedEntities = new List<HostNetworkEntity>();

    void Start()
    {
        if (Instance == null && IsHosting())
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Debug.LogError("HostNetworkManager already exists or this client is not a host.");
            Destroy(this);
        }
    }

    override protected void Tick()
    {
        // send updated state of replicated entities
        EntityStatesPacket entityStatesPacket = CreateEntityStatesPacket(replicatedEntities);

        if (entityStatesPacket != null)
        {
            SendPacketToAll(entityStatesPacket, Networking.SendType.Unreliable);
        }

        // add newly spawned entities to replicated entities list
        replicatedEntities.AddRange(spawnedEntities);

        // send spawned entities
        SpawnEntitiesPacket spawnEntitiesPacket = CreateSpawnEntitiesPacket(spawnedEntities);

        if (spawnEntitiesPacket != null)
        {
            spawnedEntities.Clear();
            SendPacketToAll(spawnEntitiesPacket, Networking.SendType.Reliable);
        }

        // send destroyed entities
        DestroyEntitiesPacket destroyEntitiesPacket = CreateDestroyEntitiesPacket(destroyedNetworkIds);

        if (destroyEntitiesPacket != null)
        {
            destroyedNetworkIds.Clear();
            SendPacketToAll(destroyEntitiesPacket, Networking.SendType.Reliable);
        }

        tickSequence++;
    }

    override public void InitializeGame()
    {
        base.InitializeGame();

        Client.Instance.Networking.OnP2PData = HandleOnP2PData;
        Client.Instance.Networking.OnConnectionFailed = HandleOnConnectionFailed;
        Client.Instance.Networking.OnIncomingConnection = HandleOnIncomingConnection;
    }

    public void NetworkEntitySpawned(HostNetworkEntity entity)
    {
        spawnedEntities.Add(entity);
    }

    public void NetworkEntityDestroyed(HostNetworkEntity entity)
    {
        destroyedNetworkIds.Add(entity.networkId);

        // no longer replicate state of this entity
        replicatedEntities.Remove(entity);
    }

    private void HandleOnConnectionFailed(ulong steamId, Networking.SessionError error)
    {
        return;
    }

    protected void HandleOnP2PData(ulong steamId, byte[] data, int length, int nChannel)
    {
        return;
    }

    private bool HandleOnIncomingConnection(ulong steamId)
    {
        return true;
    }

    public static void SendPacketToAll(object packet, Networking.SendType sendType)
    {
        foreach(ulong clientSteamId in Client.Instance.Lobby.GetMemberIDs())
        {
            SendPacket(clientSteamId, packet, sendType);
        }
    }

    protected SpawnEntitiesPacket CreateSpawnEntitiesPacket(List<HostNetworkEntity> entities)
    {
        if (entities.Count == 0)
            return null;

        SpawnEntitiesPacket spawnEntitiesPacket = new SpawnEntitiesPacket();

        spawnEntitiesPacket.tickSequence = tickSequence;
        spawnEntitiesPacket.spawnEntityPackets = new SpawnEntityPacket[entities.Count];

        for (int i = 0; i < entities.Count; i++)
        {
            spawnEntitiesPacket.spawnEntityPackets[i] = HostNetworkEntity.CreateSpawnEntityPacket(entities[i]);
        }

        return spawnEntitiesPacket;
    }

    protected DestroyEntitiesPacket CreateDestroyEntitiesPacket(List<ushort> networkIds)
    {
        if (networkIds.Count == 0)
            return null;

        DestroyEntitiesPacket destroyEntitiesPacket = new DestroyEntitiesPacket();

        destroyEntitiesPacket.tickSequence = tickSequence;
        destroyEntitiesPacket.destroyEntityPackets = new DestroyEntityPacket[networkIds.Count];

        for (int i = 0; i < networkIds.Count; i++)
        {
            destroyEntitiesPacket.destroyEntityPackets[i] = HostNetworkEntity.CreateDestroyEntityPacket(networkIds[i]);
        }

        return destroyEntitiesPacket;
    }

    protected EntityStatesPacket CreateEntityStatesPacket(List<HostNetworkEntity> entities)
    {
        if (entities.Count == 0)
            return null;

        EntityStatesPacket entityStatesPacket = new EntityStatesPacket();

        entityStatesPacket.tickSequence = tickSequence;
        entityStatesPacket.entityStatePackets = new EntityStatePacket[entities.Count];

        for (int i = 0; i < entities.Count; i++)
        {
            entityStatesPacket.entityStatePackets[i] = HostNetworkEntity.CreateEntityStatePacket(entities[i]);
        }

        return entityStatesPacket;
    }
}

[System.SerializableAttribute()]
public class EntityStatesPacket
{
    public ushort tickSequence;
    public EntityStatePacket[] entityStatePackets;
}

[System.SerializableAttribute()]
public class SpawnEntitiesPacket
{
    public ushort tickSequence;
    public SpawnEntityPacket[] spawnEntityPackets;
}

[System.SerializableAttribute()]
public class DestroyEntitiesPacket
{
    public ushort tickSequence;
    public DestroyEntityPacket[] destroyEntityPackets;
}