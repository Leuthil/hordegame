﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NetGraph : MonoBehaviour
{
    public Text TxtNumberOfRemoteConnections;
    public Text TxtPing;
    public Text TxtTotalIncomingMessages;
    public Text TxtTotalIncomingBytes;
    public Text TxtTotalOutgoingMessages;
    public Text TxtTotalOutgoingBufferedMessages;
    public Text TxtTotalOutgoingBytes;
    public Text TxtTotalOutgoingMessagesBufferedPerSecond;

    protected void Start()
    {
        DontDestroyOnLoad(this.gameObject);
    }

    protected void Update ()
    {
        TxtNumberOfRemoteConnections.text = "Connection Count: " + NetGraphStats.NumberOfRemoteConnections;
        TxtPing.text = "Ping: " + NetGraphStats.Ping;
        TxtTotalIncomingMessages.text = "Inc. Msgs: " + NetGraphStats.TotalIncomingMessages;
        TxtTotalIncomingBytes.text = "Inc Bytes: " + NetGraphStats.TotalIncomingBytes;
        TxtTotalOutgoingMessages.text = "Out Msgs: " + NetGraphStats.TotalOutgoingMessages;
        TxtTotalOutgoingBufferedMessages.text = "Out Buffered Msgs: " + NetGraphStats.TotalOutgoingBufferedMessages;
        TxtTotalOutgoingBytes.text = "Out Bytes: " + NetGraphStats.TotalOutgoingBytes;
        TxtTotalOutgoingMessagesBufferedPerSecond.text = "Out Msgs Buffered / s: " + NetGraphStats.TotalOutgoingMessagesBufferedPerSecond;
    }
}
