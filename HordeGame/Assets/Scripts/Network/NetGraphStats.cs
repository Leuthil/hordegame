﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class NetGraphStats : MonoBehaviour
{
    public static int NumberOfRemoteConnections { get { return _numberOfRemoteConnections; } }
    public static int Ping { get { return _ping; } }
    public static int TotalIncomingMessages { get { return _totalIncomingMessages; } }
    public static int TotalIncomingBytes { get { return _totalIncomingBytes; } }
    public static int TotalOutgoingMessages { get { return _totalOutgoingMessages; } }
    public static int TotalOutgoingBufferedMessages { get { return _totalOutgoingBufferedMessages; } }
    public static int TotalOutgoingBytes { get { return _totalOutgoingBytes; } }
    public static int TotalOutgoingMessagesBufferedPerSecond { get { return _totalOutgoingMessagesBufferedPerSecond; } }

    // useful for server
    protected static int _numberOfRemoteConnections;

    // useful for client
    protected static int _ping;

    // shared between both
    protected static int _totalIncomingMessages;
    protected static int _totalIncomingBytes;
    protected static int _totalOutgoingMessages;
    protected static int _totalOutgoingBufferedMessages;
    protected static int _totalOutgoingBytes;
    protected static int _totalOutgoingMessagesBufferedPerSecond;

    protected void Update()
    {
        if (NetworkManager.singleton == null || !NetworkManager.singleton.isNetworkActive)
        {
            _numberOfRemoteConnections = 0;
            _ping = 0;
            _totalIncomingMessages = 0;
            _totalIncomingBytes = 0;
            _totalOutgoingMessages = 0;
            _totalOutgoingBufferedMessages = 0;
            _totalOutgoingBytes = 0;
            _totalOutgoingMessagesBufferedPerSecond = 0;

            return;
        }

        if (NetworkServer.active)
        {
            NetworkServer.GetStatsIn(out _totalIncomingMessages, out _totalIncomingBytes);
            NetworkServer.GetStatsOut(out _totalOutgoingMessages, out _totalOutgoingBufferedMessages, out _totalOutgoingBytes, out _totalOutgoingMessagesBufferedPerSecond);

            _numberOfRemoteConnections = 0;
            foreach(var client in NetworkServer.connections)
            {
                if (client != null)
                {
                    _numberOfRemoteConnections++;
                }
            }

            _ping = 0;
        }
        else if (NetworkClient.active)
        {
            NetworkManager.singleton.client.GetStatsIn(out _totalIncomingMessages, out _totalIncomingBytes);
            NetworkManager.singleton.client.GetStatsOut(out _totalOutgoingMessages, out _totalOutgoingBufferedMessages, out _totalOutgoingBytes, out _totalOutgoingMessagesBufferedPerSecond);

            _numberOfRemoteConnections = 1;

            _ping = NetworkManager.singleton.client.GetRTT();
        }
        else
        {
            _numberOfRemoteConnections = 0;
            _ping = 0;
            _totalIncomingMessages = 0;
            _totalIncomingBytes = 0;
            _totalOutgoingMessages = 0;
            _totalOutgoingBufferedMessages = 0;
            _totalOutgoingBytes = 0;
            _totalOutgoingMessagesBufferedPerSecond = 0;
        }
    }
}
