﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HostNetworkEntity : NetworkEntity
{
    protected static ushort idCount = 0;

    void Start()
    {
        idCount++;
        networkId = idCount;

        HostNetworkManager.Instance.NetworkEntitySpawned(this);
    }

    void OnDestroy()
    {
        HostNetworkManager.Instance.NetworkEntityDestroyed(this);
    }

    public static EntityStatePacket CreateEntityStatePacket(HostNetworkEntity entity)
    {
        if (entity == null)
        {
            Debug.LogError("Trying to create a state packet for an entity that is null");
            return null;
        }

        EntityStatePacket entityStatePacket = new EntityStatePacket();

        entityStatePacket.networkId = entity.networkId;
        entityStatePacket.health = entity.health;
        entityStatePacket.shield = entity.shield;
        entityStatePacket.targetId = entity.targetId;
        entityStatePacket.statuses = entity.statuses;
        entityStatePacket.x = entity.x;
        entityStatePacket.z = entity.z;

        return entityStatePacket;
    }

    public static SpawnEntityPacket CreateSpawnEntityPacket(HostNetworkEntity entity)
    {
        if (entity == null)
        {
            Debug.LogError("Trying to create a spawn packet for an entity that is null");
            return null;
        }

        SpawnEntityPacket spawnEntityPacket = new SpawnEntityPacket();

        spawnEntityPacket.networkId = entity.networkId;
        spawnEntityPacket.type = entity.type;
        spawnEntityPacket.health = entity.health;
        spawnEntityPacket.shield = entity.shield;
        spawnEntityPacket.targetId = entity.targetId;
        spawnEntityPacket.statuses = entity.statuses;
        spawnEntityPacket.x = entity.x;
        spawnEntityPacket.z = entity.z;

        return spawnEntityPacket;
    }

    public static DestroyEntityPacket CreateDestroyEntityPacket(ushort networkId)
    {
        DestroyEntityPacket destroyEntityPacket = new DestroyEntityPacket();

        destroyEntityPacket.networkId = networkId;

        return destroyEntityPacket;
    }
}

[System.SerializableAttribute()]
public class EntityStatePacket
{
    public ushort networkId;
    public ushort health;
    public ushort shield;
    public ushort targetId;
    public byte statuses;
    public ushort x;
    public ushort z;
}

[System.SerializableAttribute()]
public class SpawnEntityPacket
{
    public ushort networkId;
    public ushort type;
    public ushort health;
    public ushort shield;
    public ushort targetId;
    public byte statuses;
    public ushort x;
    public ushort z;
}

[System.SerializableAttribute()]
public class DestroyEntityPacket
{
    public ushort networkId;
}