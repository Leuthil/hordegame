﻿using UnityEngine;
using System.Collections;
using Facepunch.Steamworks;
using System;
using System.Collections.Generic;

public abstract class COTANetworkManager : MonoBehaviour
{
    protected static readonly uint TICKRATE = 15;

    protected float inverseTickrate = 1f / (float)TICKRATE;
    protected float timeSinceLastTick = 0f;

    protected ushort tickSequence = 0;

    void FixedUpdate()
    {
        timeSinceLastTick += Time.deltaTime;

        while (timeSinceLastTick >= inverseTickrate)
        {
            Tick();
            timeSinceLastTick -= inverseTickrate;
        }
    }

    abstract protected void Tick();

    virtual public void InitializeGame()
    {
        timeSinceLastTick = 0f;
        tickSequence = 0;

        Client.Instance.Networking.SetListenChannel(0, true);
    }

    public static bool IsHosting()
    {
        return Client.Instance.Lobby.IsValid && Client.Instance.Lobby.Owner == Client.Instance.SteamId;
    }

    public static void SendPacket(ulong destinationSteamId, object packet, Networking.SendType sendType)
    {
        if (destinationSteamId == Client.Instance.SteamId)
            return;

        byte[] packetData = EasySerializer.SerializeObjectToBinary(packet);

        Client.Instance.Networking.SendP2PPacket(destinationSteamId, packetData, packetData.Length, sendType);
    }
}