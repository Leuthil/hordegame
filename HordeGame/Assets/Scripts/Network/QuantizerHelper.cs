﻿using System;

public static class QuantizerHelper
{
    public static float posXCompressionMax = 19f;
    public static float posXCompressionFactor = ushort.MaxValue / posXCompressionMax;
    public static float posYCompressionMax = 21f;
    public static float posYCompressionFactor = ushort.MaxValue / posYCompressionMax;
    public static float rotCompressionMax = 360f;
    public static float rotCompressionFactor = byte.MaxValue / rotCompressionMax;
    
    // Maps a float value range to another float value ranger
    public static float Map(float value, float oldMin, float oldMax, float newMin, float newMax)
    {
        return (((value - oldMin) * (newMax - newMin)) / (oldMax - oldMin)) + newMin;
    }

    public static ushort FloatToUShort(float value, float minValue, float maxValue)
    {
        return (ushort)Map(value, minValue, maxValue, ushort.MinValue, ushort.MaxValue);
    }
    
    public static float UShortToFloat(ushort value, float minValue, float maxValue)
    {
        return Map(value, ushort.MinValue, ushort.MaxValue, minValue, maxValue);
    }
    
    public static ushort FloatToByte(float value, float minValue, float maxValue)
    {
        return (byte)Map(value, minValue, maxValue, byte.MinValue, byte.MaxValue);
    }
    
    public static float ByteToFloat(byte value, float minValue, float maxValue)
    {
        return Map(value, byte.MinValue, byte.MaxValue, minValue, maxValue);
    }

    public static ushort PositionXFloatToUShort(float f)
    {
        return (ushort)Math.Round(f * posXCompressionFactor);
    }

    public static float PositionXUShortToFloat(ushort s)
    {
        return (float)s / posXCompressionFactor;
    }

    public static ushort PositionYFloatToUShort(float f)
    {
        return (ushort)Math.Round(f * posYCompressionFactor);
    }

    public static float PositionYUShortToFloat(ushort s)
    {
        return (float)s / posYCompressionFactor;
    }

    public static byte RotationFloatToByte(float f)
    {
        if (f <= -360f) f = -(Math.Abs(f) % 360f);
        if (f < 0f) f += 360f;
        if (f > 360f) f = f % 360f;
        return (byte)Math.Round(f * rotCompressionFactor);
    }

    public static float RotationByteToFloat(byte b)
    {
        return (float)b / rotCompressionFactor;
    }

    public static bool GetBit(byte b, int pos)
    {
        return ((b & (byte)(1 << pos)) != 0);
    }

    public static byte SetBit(byte b, int pos)
    {
        return (byte)(b | (byte)(1 << pos));
    }
}