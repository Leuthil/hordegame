﻿using UnityEngine;
using System.Collections;
using Facepunch.Steamworks;
using UnityEngine.UI;
using System.Collections.Generic;
using System;

public class SteamLobbyManager : MonoBehaviour
{
    protected Canvas messageCanvas;
    protected Dropdown lobbiesDropdown;

    protected Dictionary<int, ulong> lobbiesDropdownMap;

    void Start()
    {
        messageCanvas = GameObject.Find("MessageCanvas").GetComponent<Canvas>();
        lobbiesDropdown = GameObject.Find("LobbiesDropdown").GetComponent<Dropdown>();

        if (!Client.Instance.IsValid)
        {
            Debug.LogError("Client instance is not yet valid");
            return;
        }

        Client.Instance.LobbyList.OnLobbiesUpdated = HandleOnLobbiesUpdated;
        Client.Instance.Lobby.OnLobbyCreated = HandleOnLobbyCreated;
        Client.Instance.Lobby.OnLobbyJoined = HandleOnLobbyJoined;
        Client.Instance.Lobby.OnLobbyStateChanged = HandleOnLobbyStateChanged;

        RefreshLobbies();
    }

    protected void HandleOnLobbyStateChanged(Lobby.MemberStateChange stateChange, ulong initiaterSteamID, ulong affectedSteamID)
    {
        Debug.Log("lobby state changed: " + stateChange.ToString());
        return;
    }

    protected void HandleOnLobbyJoined(bool success)
    {
        if (success)
        {
            Debug.Log("lobby joined: " + Client.Instance.Lobby.CurrentLobby);

            if (!COTANetworkManager.IsHosting())
            {
                //ClientNetworkManager.Instance.InitializeGame();
            }
        }
        else
        {
            Debug.LogWarning("Error joining lobby");
        }
    }

    protected void HandleOnLobbyCreated(bool success)
    {
        if (success)
        {
            Debug.Log("lobby created: " + Client.Instance.Lobby.CurrentLobby);
            Client.Instance.Lobby.Name = Client.Instance.Username + "'s Game";

            if (COTANetworkManager.IsHosting())
            {
                HostNetworkManager.Instance.InitializeGame();
            }
        }
        else
        {
            Debug.Log("Error creating lobby");
        }
    }

    protected void HandleOnLobbiesUpdated()
    {
        if (Client.Instance.LobbyList.Finished)
        {
            List<LobbyList.Lobby> lobbyList = Client.Instance.LobbyList.Lobbies;
            List<Dropdown.OptionData> optionsData = new List<Dropdown.OptionData>(lobbyList.Count);
            lobbiesDropdownMap = new Dictionary<int, ulong>(lobbyList.Count);

            lobbiesDropdown.ClearOptions();

            for(int i = 0; i < lobbyList.Count; i++)
            {
                LobbyList.Lobby lobby = lobbyList[i];
                lobbiesDropdownMap.Add(i, lobby.LobbyID);
                optionsData.Insert(i, new Dropdown.OptionData(
                    String.IsNullOrEmpty(lobby.Name) ? lobby.LobbyID.ToString() : lobby.Name
                ));
            }

            lobbiesDropdown.AddOptions(optionsData);

            Debug.Log("lobby list refreshed");
        }
    }

    public void JoinSelectedLobby()
    {
        if (lobbiesDropdown.options.Count <= lobbiesDropdown.value || !lobbiesDropdownMap.ContainsKey(lobbiesDropdown.value))
        {
            Debug.LogWarning("Cannot join lobby because the one selected is invalid");
            return;
        }

        JoinLobby(lobbiesDropdownMap[lobbiesDropdown.value]);
    }

    public void JoinLobby(ulong lobbyID)
    {
        if (Client.Instance.Lobby.IsValid)
        {
            Debug.LogWarning("Cannot join a lobby because you are already in one");
            return;
        }

        Client.Instance.Lobby.Join(lobbyID);
    }

    public void LeaveLobby()
    {
        if (Client.Instance.Lobby.IsValid)
        {
            Client.Instance.Lobby.Leave();
        }
    }

    public void CreateLobby()
    {
        if (Client.Instance.Lobby.IsValid)
        {
            Debug.LogWarning("Cannot create a lobby because you are already in one!");
            return;
        }

        Client.Instance.Lobby.Create(Lobby.Type.Public, 2);
    }

    public void RefreshLobbies()
    {
        Client.Instance.LobbyList.Refresh();
    }

    public void StartGame()
    {
        if (!Client.Instance.Lobby.IsValid)
        {
            Debug.LogWarning("Cannot start game because you are not in a lobby");
            return;
        }

        if (Client.Instance.Lobby.Owner != Client.Instance.SteamId)
        {
            Debug.LogWarning("Cannot start game because you are not the lobby owner");
            return;
        }

        messageCanvas.enabled = false;

        // GameManager.Instance.StartGame();
    }
}
