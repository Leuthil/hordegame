﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PlayerInput : NetworkBehaviour
{
    public float speed = 50f;

    protected Rigidbody rb;
    protected bool pressingUp;
    protected bool pressingLeft;
    protected bool pressingDown;
    protected bool pressingRight;

    protected void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    protected void Update ()
    {
		if (!isLocalPlayer)
        {
            return;
        }

        SetInputs();
        Move();
	}

    protected void Move()
    {
        Vector3 movement = new Vector3(0f, 0f, 0f);

        if (pressingUp)
        {
            movement.z += 1f;
        }
        if (pressingLeft)
        {
            movement.x -= 1f;
        }
        if (pressingDown)
        {
            movement.z -= 1f;
        }
        if (pressingRight)
        {
            movement.x += 1f;
        }

        rb.MovePosition(rb.position + movement.normalized * speed * Time.deltaTime);
    }

    protected void SetInputs()
    {
        pressingUp = false;
        pressingLeft = false;
        pressingDown = false;
        pressingRight = false;

        if (Input.GetKey(KeyCode.W))
        {
            pressingUp = true;
        }
        if (Input.GetKey(KeyCode.A))
        {
            pressingLeft = true;
        }
        if (Input.GetKey(KeyCode.S))
        {
            pressingDown = true;
        }
        if (Input.GetKey(KeyCode.D))
        {
            pressingRight = true;
        }
    }
}
